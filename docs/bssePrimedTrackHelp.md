# Primed Track :: Documentation

## Requirements

Primed Track requires several tools:

* Imaris:
	* ImarisXT (IPSS)
	* Imaris MeasurementPro (Reference Frame)
	* Imaris Track
	* Imaris Lineage
* MATLAB:
	* IceImarisConnector: https://github.com/aarpon/IceImarisConnector

For the (optional) tree validation, the following is required:

* Python 2.7:
	* pIceImarirConnector: https://github.com/aarpon/pIceImarisConnector/releases/latest
	* Zhang-Shasha Tree edit distance (https://zhang-shasha.readthedocs.io/en/latest): `python -m pip install zss`
	* treelib: `python -m pip install treelib`
	* numpy: `python -m pip install numpy`
	* scipy: `python -m pip install scipy`

## Usage

### Cell segmetation

* Detect green and red cells using the **Spot detector** in Imaris. Use a low threshold to segment all cells even at the cost of including spurious spots. Allow spot radius to be adapted to more accurately fit the volume of the segmented cell. Bright but very small spots can easily be filtered out during segmentation.

### First validation

#### Using the "BSSE > Primed Track > Validate cells in embryo" ImarisXTension (MATLAB) 

* Use the green spot positions to estimate the embryo diameter and discard green spots that are likely to be outside of the embryo. The radius of the embryo is roughly estimated as the median of all maximal inter-spot distances.

![Select the green cells](./images/validate_1.png){width=400px}

* Search for spots that occur within a small defined distance from a spot in the same channel, discard all wrongly segmented double spots on one nucleus and replace them by one new spot.

![Resolve double segmentations](./images/validate_2.png){width=265px}

* A user-defined multiplicative factor can optionally compensate for estimation errors and prevent cells at the boundary of the embryo to be discarded should this constraint be too stringent.

![Correct for understimation of embryo diameter](./images/validate_3.png){width=265px}

* Discard all spurious red spots that do not colocalize with a green spot. Note that due to the equilibrium between protonated and de-protonated chromophore, green to red photoconversion of pcFPs is never exhaustive and will always retain a green population, rendering this quality control step possible.

![Remove red spots that do not overlap with green spots](./images/validate_4.png){width=265px}

* A red spot discarded during the first validation can optionally be recovered if there is a valid red spot in previous time point within a user-defined search radius. This adjustment compensates for remaining miss-segmentations in the green channel.

![Recover discarded red spots](./images/validate_5.png){width=265px}

### Embryo alignment (drift and rotational correction) / Cropping

#### Using the "BSSE > Primed Track > Set reference frames from cells" ImarisXTension (MATLAB) 

*	Imaris Reference Frame Objects are created in MATLAB for each time point: their origin is set at the position of the center of mass (COM) of the green spots and their orientation is given by the vector.

![Select processed green spots](./images/reference_frame_1.png){width=250px}

![Select processed red spots](./images/reference_frame_2.png){width=130px}

*	Perform resampling using the reference frame object in Imaris.

![Select created reference frame in Imaris](./images/reference_frame_3.png)

![Perform registration](./images/reference_frame_4.png)

#### Using the "BSSE > Primed Track > Correct rotation around reference frame axis" ImarisXTension (MATLAB)

* The previous correction still has one degree of freedom. The rotation angle around the reference frame axis is obtained by comparing the positions of the green spots at timepoints t and t - 1 over 360 1-degree rotations and by choosing the angle that minimizes the cell drift between time points.

![Select the processed green spot](./images/rotation_1.png){width=250px}

* Perform resampling using the reference frame object in Imaris and crop the result to the smallest bounding volume.

![Select created reference frame in Imaris](./images/rotation_2.png)

![Perform registration](./images/rotation_3.png)

### Second validation and subsetting

#### Using the "BSSE > Primed Track > Validate cells in embryo" ImarisXTension (MATLAB)

* To pick up red cells that were not recovered previously, re-run the first validation on the re-aligned embryo (follow the steps from above).
* Create a new spot object that contains the subset of green cells that do not colocalize with red cells.

![Subsetting](./images/subsetting_1.png)

### Lineage tree reconstruction

* Imaris’ Lineage module is used to track the cells over time and reconstruct their lineage tree. The subsetting in the previous step allows us to reduce the complexity of the lineage tracing problem by breaking it down into two simpler, computationally less expansive, disjoint problems.

![Lineage tree reconstruction](./images/lineage_reconstruction_1.png)

### Comparative Analysis of lineage trees (optional)

#### Using the "BSSE > Primed Track > Calculate total distance between lineage trees" ImarisXTension (python)

* Run the python XTension to calculate the total distance between lineage trees before and after correction.

![First lineage tree](./images/tree_distance_1.png){width=300px}

![Second lineage tree](./images/tree_distance_2.png){width=300px}

![Output of the lineage tree distance calculation](./images/tree_distance_3.png){width=400px}