# Collect arguments
DOCUMENT=$1
INPUT_FILE_LIST_TXT=${DOCUMENT}.txt
OUTPUT_TEX_FILE=${DOCUMENT}.tex
OUTPUT_PDF_FILE=${DOCUMENT}.pdf

# Inform
echo "Processing ${INPUT_FILE_LIST_TXT} to produce ${OUTPUT_TEX_FILE} and ${OUTPUT_PDF_FILE}."

# Markdown to Latex
echo "Converting Markdown to Latex..."
pandoc                               \
  --from         markdown            \
  --to           latex               \
  --standalone                       \
  --template     template_no_toc.tex \
  --out          ${OUTPUT_TEX_FILE}  \
  --pdf-engine xelatex               \
  --toc                              \
  `cat ${INPUT_FILE_LIST_TXT}`

# Making figures non-floating
# In Linux, change "sed -i '' -e ..." to "sed -i ..."
echo "Making figures non-floating..."
in="\begin{figure}"
out="\begin{figure}[H]"
sed -i '' -e "s/${in}/${out}/g" ${OUTPUT_TEX_FILE}

# Add the date
echo "Set the date..."
in="\date{}"
out="\date{`date +%Y-%m-%d`}"
sed -i '' -e "s/${in}/${out}/g" ${OUTPUT_TEX_FILE}

# Converting to PDF
echo "Converting to PDF..."
xelatex ${OUTPUT_TEX_FILE}
xelatex ${OUTPUT_TEX_FILE}

# Move to dist directory
if [ ! -d "dist" ]; then
  mkdir dist
fi
mv ./${OUTPUT_PDF_FILE} ./dist/${OUTPUT_PDF_FILE}

# Clean up
rm -f ${DOCUMENT}.aux ${DOCUMENT}.log ${DOCUMENT}.out ${DOCUMENT}.toc ${DOCUMENT}.tex

echo "All done."

