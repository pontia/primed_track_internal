# Export TYXZC dataset from HDF5 file to series of TIFF stacks

import numpy as np
from tifffile import imsave
import h5py

h5_filename = r'E:\tracking_results.h5'
dataset_name = "/exported_data"
n_timepoints = 264

file = h5py.File(h5_filename, 'r')
for i in range(n_timepoints):

    # Get ith stack
    stack = np.squeeze(file[dataset_name][i])
    stack = np.swapaxes(stack, 0, 2)
    
    # Change to u8
    stack = stack.astype(np.uint8)

    # How many unique IDs?
    IDs = np.unique(stack.ravel())

    # Output file name
    out_filename = r'E:\tracking' + str(i).zfill(3) + ".tif"
    imsave(out_filename, stack)

    print("Processed time point " + str(i))

file.close()

print("Done!")