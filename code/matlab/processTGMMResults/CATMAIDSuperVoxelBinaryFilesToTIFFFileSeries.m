function CATMAIDSuperVoxelBinaryFilesToTIFFFileSeries(inDirName, outDirName, fileNameFilter)
% This function converts supervoxel binary files (SVB) to TIFF series.
%
% The data is created with the TGMM software by Philipp Keller [1] and read
% into MATLAB with the readListSupervoxelsFromBinaryFile()
% function from CATMAID MATLAB [2].
% 
%    [posXYZ, timeIndices, radii, trackEdges] = ...
%         CATMAIDTrackingMatrixToImarisSpotData(trackingMatrix)
%    newSpot = conn.createAndSetSpots(posXYZ, timeIndices, radii, ...
%         'TGMM', rand(1, 4));
%    newSpot.SetTrackEdges(trackEdges);
%
% [1] http://www.janelia.org/sites/default/files/Labs/Keller%20Lab/TGMM_Supplementary_Software_1_0.zip
% [2] https://bitbucket.org/fernandoamat/catmaid-matlab-code
%
% INPUT
%
%    inDirName     : folder containing the SVB files.
%    outDirName    : folder where to save the generated TIFF files.
%    fileNameFilter: filter for the file names; omit to use the default:
%                    'GMEMfinalResult_frame*.svb'. Make sure to use '*'
%                    as a placeholder for the numerical index.
% 
% Aaron Ponti, 2018-02-02

if nargin == 2
    fileNameFilter = 'GMEMfinalResult_frame*.svb';
end

% Get all files
d = dir(fullfile(inDirName, fileNameFilter));
if isempty(d)
    fprintf(1, 'No files found!\n');
    return
end

% Get the file names
fileNames = cell(1, numel(d));
for i = 1 : numel(d)
    fileNames{i} = d(i).name;
end

% Sort them
fileNames = sort(fileNames);
nFiles = numel(fileNames);
if nFiles == 0
    fprintf(1, 'No files found!\n');
    return
end

% Fall-back formatting string
strg = sprintf('%%.%dd', length(num2str(nFiles)));

% Build segmentation images
for k = 1 : nFiles
   
    % Make sure that the input and output files havethe same numeric index
    [~, body] = fileparts(fileNames{k});
    tokens = regexp(body, '(\d+)$', 'tokens');
    if isempty(tokens)
        numSuffix = sprintf(strg, k);
        fprintf(1, 'Could not find numeric index! Defaulting to %s!\n', ...
            numSuffix);
    else
        numSuffix = tokens{1}{1};
    end
    
    % Read the supervoxel binary file
    [svList, sizeIm] = ...
        readListSupervoxelsFromBinaryFile(fullfile(inDirName, fileNames{k}));

    % Build the black and white mask
    stack = zeros(sizeIm, 'uint8');
    for i = 1 : size(svList, 1)
        stack(svList{i}) = uint8(255);
    end

    % Swap y and z dimensions
    sw_stack = zeros([size(stack, 2), size(stack, 1), size(stack, 3)], ...
        class(stack));
    for i = 1 : size(stack, 3)
       sw_stack(:, :, i) = stack(:, :, i)'; 
    end
    
    % Write the multi-plane TIFF
    outFileName = fullfile(outDirName, ['T', numSuffix, '.tif']);
    for j = 1 : sizeIm(3)
        if j == 1
            writeMode = 'overwrite';
        else
            writeMode = 'append';
        end
        imwrite(sw_stack(:, :, j), outFileName, 'WriteMode', writeMode);
    end
    
    fprintf(1, 'Written file %s.\n', outFileName);

end

