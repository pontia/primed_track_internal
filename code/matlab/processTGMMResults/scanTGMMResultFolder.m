function [fileBaseName, xmlFiles, svbFiles] = scanTGMMResultFolder(inDirName, fileNameFilter)
% This function returns file info from a TGMM result folder.
%
% The folder stores results is created with the TGMM software by 
% Philipp Keller [1].
% 
% [1] http://www.janelia.org/sites/default/files/Labs/Keller%20Lab/TGMM_Supplementary_Software_1_0.zip
%
% INPUT
%
%    inDirName     : folder containing the TGMM results.
%    fileNameFilter: filter for the file names; omit to use the default:
%                    'GMEMfinalResult_frame*'. Make sure to use '*'
%                    as a placeholder for the numerical index.
% 
% OUTPUT
% 
%    fileBaseName  : common base name, without numeric index and extension
%    xmlFiles      : sorted list of xml Files
%    svbFiles      : sorted list of svb (Super-Voxel Binary) Files
%
% Aaron Ponti, 2018-02-02

if nargin == 1
    fileNameFilter = 'GMEMfinalResult_frame*';
end

% Get the XML files
d = dir(fullfile(inDirName, cat(2, fileNameFilter, '.xml')));
xmlFiles = getSortedFileNameList(d);

% Get the SVB files
d = dir(fullfile(inDirName, cat(2, fileNameFilter, '.svb')));
svbFiles = getSortedFileNameList(d);

% Get the base name
index = strfind(fileNameFilter, '*');
fileBaseName = fileNameFilter(1 : index - 1);

function sortedList = getSortedFileNameList(d)

% Get the file names
sortedList = cell(1, numel(d));
for i = 1 : numel(d)
    sortedList{i} = d(i).name;
end

% Sort them
sortedList = sort(sortedList);
