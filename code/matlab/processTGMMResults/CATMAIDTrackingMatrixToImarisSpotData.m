function [posXYZ, timeIndices, radii, trackEdges] = CATMAIDTrackingMatrixToImarisSpotData(trackingMatrix)
% This function returns data that can be feed to Imaris ISpots objects to
% create spots with tracks and lineage traces.
%
% The data is created with the TGMM software by Philipp Keller [1] and read
% into MATLAB with the parseMixtureGaussiansXml2trackingMatrixCATMAIDformat()
% function from CATMAID MATLAB [2] to return the trackingMatrix that is 
% fed into this function.
% 
% With IceImarisConnector [3], a new ISpots object can be generated as
% follows:
%
%    [posXYZ, timeIndices, radii, trackEdges] = ...
%         CATMAIDTrackingMatrixToImarisSpotData(trackingMatrix)
%    newSpot = conn.createAndSetSpots(posXYZ, timeIndices, radii, ...
%         'TGMM', rand(1, 4));
%    newSpot.SetTrackEdges(trackEdges);
%
% [1] http://www.janelia.org/sites/default/files/Labs/Keller%20Lab/TGMM_Supplementary_Software_1_0.zip
% [2] https://bitbucket.org/fernandoamat/catmaid-matlab-code
% [3] https://github.com/aarpon/IceImarisConnector
%
% Aaron Ponti, 2018-02-02

% Spot positions
posXYZ = single(trackingMatrix(:, 3:5));

% Time indices
timeIndices = getTimeIndicesWithCorrectOffset(trackingMatrix(:, 8));

% Radii
radii = trackingMatrix(:, 6);
mn = mean(radii(radii ~= -1));
if mn == -1
    radii(radii == -1) = 1.0;
else
    radii(radii == -1) = mn;
end

% Edges
trackEdges = trackingMatrix(:, [7 1]);
trackEdges = trackEdges - 1;
trackEdges(trackEdges(:, 1) == -2, :) = [];

% ===

function timeIndices = getTimeIndicesWithCorrectOffset(timeIndices)

if min(timeIndices(timeIndices > -1)) > 0
    timeIndices = timeIndices - 1;
    timeIndices(timeIndices == -2) = -1;
end
