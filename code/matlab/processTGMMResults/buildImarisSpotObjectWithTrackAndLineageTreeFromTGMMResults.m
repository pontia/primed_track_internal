function buildImarisSpotObjectWithTrackAndLineageTreeFromTGMMResults( ...
    conn, inDirName)

% Get the file names
[fileBaseName, xmlFiles, ~] = scanTGMMResultFolder(inDirName, 'GMEMfinalResult_frame*');

% Extract tracking information
trackingMatrix = parseMixtureGaussiansXml2trackingMatrixCATMAIDformat(fullfile(inDirName, fileBaseName), 0, numel(xmlFiles) - 1);

% Create a tracked spot object
[posXYZ, timeIndices, radii, trackEdges] = CATMAIDTrackingMatrixToImarisSpotData(trackingMatrix);

% Get and correct the voxel size
[vx, vy, vz] = conn.getVoxelSizes();
posXYZ(:, 1) = posXYZ(:, 1) * vx;
posXYZ(:, 2) = posXYZ(:, 2) * vy;
posXYZ(:, 3) = posXYZ(:, 3) * vz;

% Subtract the offset
[ex, ~, ey, ~, ez, ~] = conn.getExtends();
posXYZ(:, 1) = posXYZ(:, 1) - ex;
posXYZ(:, 2) = posXYZ(:, 2) - ey;
posXYZ(:, 3) = posXYZ(:, 3) - ez;

% Add the spots
newSpot_adv = conn.createAndSetSpots(posXYZ, timeIndices, radii, ...
    'TGMM_advanced', [rand(1, 3) 0]);
newSpot_adv.SetTrackEdges(trackEdges);
