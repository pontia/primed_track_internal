function buildImarisSpotObjectWithTrackAndLineageTreeFromIlastikResults( ...
    conn, inDirName)

minSize = 100;

% Get the file names
d = dir(inDirName);
fileNames = {};
n = 0;
for i = 1 : numel(d)
    
    if d(i).isdir == 1
        continue;
    end
    
    if ~ strcmpi(d(i).name(end - 3 : end), '.tif')
        continue;
    end
    
    n = n + 1;
    fileNames{n} = fullfile(inDirName, d(i).name);
    
end
   
fileNames = sort(fileNames);

% Initialize spot data
posXYZ = [];
timeIndices = [];
radii = [];
IDs = [];

% Initialize waitbar
hWaitbar = waitbar(0, 'Processing files...');

% Process the files
for i = 1 : numel(fileNames)
    
    fprintf(1, 'Processing file %s...\n', fileNames{i});

    % Read the file
    stack = readstack(fileNames{i});

    % Extract spot positions and track edges
    [cPosXYZ, cIDs] = getSpotPosXYZAndIDs(stack, minSize, i);
    
    % Append to global arrays
    posXYZ = cat(1, posXYZ, cPosXYZ);
    timeIndices = cat(1, timeIndices, (i - 1) .* ones(size(cIDs)));
    radii = cat(1, radii, 5.0 .* ones(size(cIDs)));
    IDs = cat(1, IDs, cIDs);

    % Update waitbar
    waitbar(i / numel(fileNames), hWaitbar);

end

% Close waitbar
close(hWaitbar);

% Build trackEdges
trackEdges = buildTrackEdges(IDs, timeIndices);

% Get and correct the voxel size
[vx, vy, vz] = conn.getVoxelSizes();
posXYZ(:, 1) = posXYZ(:, 1) * vx;
posXYZ(:, 2) = posXYZ(:, 2) * vy;
posXYZ(:, 3) = posXYZ(:, 3) * vz;

% Subtract the offset
[ex, ~, ey, ~, ez, ~] = conn.getExtends();
posXYZ(:, 1) = posXYZ(:, 1) - ex;
posXYZ(:, 2) = posXYZ(:, 2) - ey;
posXYZ(:, 3) = posXYZ(:, 3) - ez;

% Add the spots
newSpot_adv = conn.createAndSetSpots(posXYZ, timeIndices, radii, ...
    'From ilastik', [rand(1, 3) 0]);
newSpot_adv.SetTrackEdges(trackEdges);

function stack = readstack(filename)

stack = imread(filename, 1);

more = true;

n = 2;
while more == true
   
    try
        tmp = imread(filename, n);
    catch
        tmp = [];
    end
    
    if ~isempty(tmp)
        stack = cat(3, stack, tmp);
    else
        more = false;
    end
    n = n + 1;
end

function trackEdges = buildTrackEdges(IDs, timeIndices)

uTimeIndices = unique(timeIndices);
if (any(diff(uTimeIndices) ~= 1))
    error('There are missing time points!');
end

trackEdges = zeros(numel(IDs), 2);
uIDs = unique(IDs);

c = 0;

for i = 1 : numel(uIDs)
   
    indx = find(IDs == uIDs(i));

    c = c + 1;
    trackEdges(c, 1) = indx(1);
    
    if numel(indx) < 2
        continue;
    end
    
    for k = 2 : numel(indx) - 1
        trackEdges(c, 2) = indx(k);
        c = c + 1;
        trackEdges(c, 1) = indx(k);
    end    
    trackEdges(c, 2) = indx(k + 1);
end

% Remove tracks that contain 0s (individual spots or rows beyond the
% number of tracks)
trackEdges(trackEdges(:, 1) == 0 | trackEdges(:, 2) == 0, :) = [];

% Indices in Imaris start at 0
trackEdges = trackEdges - 1;

function [posXYZ, ids] = getSpotPosXYZAndIDs(stack, minSize, numImage)

% Get the IDs (correspond to the track edges
ids = unique(stack(:));
ids(ids == 0) = [];

posXYZ = [];

fprintf(1, '%d IDs and ', numel(ids));
    
% Extract spot positions
n = 0;
for i = 1 : numel(ids)
    
    ind = find(stack == ids(i));
    
    % Keep only large enough objects
    bw = false(size(stack));
    bw(ind) = true;
    bw = imdilate(bw, ones(5, 5, 5));
    bw = imerode(bw, ones(5, 5, 5));
%     writeStack(uint8(255) .* uint8(bw), 'E:\test.tif');
    cc = bwconncomp(bw, 26);
    n = n + cc.NumObjects;
    if cc.NumObjects > 1
        bw2 = false(size(stack));
        for j = 1 : cc.NumObjects
            bw2(cc.PixelIdxList{j}) = true;
        end
        writeStack(uint8(255.*single(bw)), ['E:\bw', num2str(numImage), '_id_', num2str(ids(i)), '.tif']);
    end
    [y, x, z] = ind2sub(size(stack), ind);
    
    X = mean(x);
    Y = mean(y);
    Z = mean(z);

    posXYZ = cat(1, posXYZ, [X, Y, Z]);

end

fprintf(1, '%d objects.\n', n);


function writeStack(s, filename)

for i = 1 : size(s, 3)
   
    if i == 1
        imwrite(s(:, :, i), filename, 'WriteMode', 'overwrite');
    else
        imwrite(s(:, :, i), filename, 'WriteMode', 'append');
    end
end

function stack = nrm(stack)
cl = class(stack);
stack = single(stack);
mn = min(stack(:));
mx = max(stack(:));
stack = single(intmax(cl)) .* (stack - mn) ./ (mx - mn);
stack = cast(stack, cl);
