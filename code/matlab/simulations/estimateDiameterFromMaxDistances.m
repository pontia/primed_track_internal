function estDiameter = estimateDiameterFromMaxDistances(embryo_diameter, cell_diameter, nCoords)

if nargin == 2
    nCoords = 1000;
end

% Diameter of embryo and cell
embryo_radius = embryo_diameter / 2;
cell_radius = cell_diameter / 2;

% Create the coordinates
coords = zeros(nCoords, 3);
n = 0;
while n < nCoords
    c = embryo_radius / sqrt(3) .* randn(1, 3);
    if norm(c, 2) < (embryo_radius - cell_radius)
        n = n + 1;
        coords(n, :)  = c;
    end
end

% Calculate all distances
D = pdist2(coords, coords);
nEntries =  sum(1:(nCoords - 1));
allD = zeros(nEntries, 1);
n = 0;
for i = 1 : nCoords
    for j = i + 1 : nCoords
        n = n + 1;
        allD(n) = D(i, j);
    end
end
hist(allD); title('Histogram of all distances');

% Extract all max distances
allMaxD = zeros(nCoords, 1);
for i = 1 : nCoords
   allMaxD(i) = max(D(i, :)); 
end
hist(allMaxD); title('Histogram of max distances');

% Return the estimated diameter
estDiameter = median(allMaxD);