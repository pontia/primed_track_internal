function merge2DTIFFsTo3D(inputFolder, outputFolder, regex)
% Merge series of 2D TIFF tiles into multi-plane TIFF files
%
% SYNOPSIS
%
%     merge2DTIFFsTo3D(inputFolder, outputFolder, regex)
%
% INPUT
%
%    inputFolder : folder containing all TIFF files to be processed. Set
%                  to '' to pick the folder via an input dialog.
%    outputFolder: folder where to store the multi-plane 3D TIFF files.
%                  Set to '' to pick the folder via an input dialog.
%    regex       : regular expression to extract the time (T) and plane 
%                  (Z) indices of the files. Optionalm default is:
%                      regex = '^.*T(\d+)_Z(\d+).tif{1,2}$';
%                  This applies to file names in the form:
%                      T0001_Z0001.tif    with any prefix.
%
% Aaron Ponti, 2017-12-18

if nargin == 2 || isempty(regex)
    regex = '^.*T(\d+)_Z(\d+).tif{1,2}$';
end
  
if isempty(inputFolder)
    inputFolder = uigetdir('.', 'Please pick the input folder');
end

% Check existence of inputfolder
if ~exist(inputFolder, 'dir')
    error(['Input folder ', inputFolder, ' does not exist.']);
end

if isempty(outputFolder)
    outputFolder = uigetdir('.', 'Please pick the output folder');
end

% Check existence of outputFolder
if ~exist(outputFolder, 'dir')
    try
        mkdir(outputFolder);
    catch
        error(['Output folder ', outputFolder, ' could not be created! Aborting...']);
    end
end

% Scan the folder
d = dir(inputFolder);

% Process the files
maxTime = -1;
files = {};
for i = 1 : numel(d)

    if d(i).isdir
        continue;
    end
       
    % Extract information from file name
    [match, tokens] = regexp(d(i).name, regex, 'match', 'tokens');
    
    if isempty(match)
        continue
    end
    
    % Time
    time = str2double(tokens{1}{1});
    plane = str2double(tokens{1}{2});
    
    % Get the list for time (if it exists)
    try
        planes = files{time};
    catch
        planes = {};
    end
    
    % Store the file at the plane index
    planes{plane} = d(i).name;

    % Put back
    files{time} = planes;

    % Keep track of the largest time point
    if time > maxTime
        maxTime = time;
    end
    
end

% Number of digits 
nDigits = max(4, length(num2str(maxTime)));
strgT = sprintf('%%.%dd', nDigits);

% Proceed to read and merge the files
for t = 1 : numel(files)
  
    % Get current time point
    planes = files{t};
    
    % Make sure the file names are sorted
    planes = sort(planes);
    
    % File name
    indxStrT = sprintf(strgT, t);
    outFileName =  fullfile(outputFolder, ['T', indxStrT, '.tif']);
    
    % Process all planes for current time point
    for z = 1 : numel(planes)
        
        % Load the file
        try
            img = imread(fullfile(inputFolder, planes{z}));
        catch
            pfrintf(1, 'Could not read file ', planes{z}, '.\n');
        end
        
        if z == 1
            imwrite(img, outFileName, 'Compression', 'none');
        else
            imwrite(img, outFileName, 'Compression', 'none', ...
                'WriteMode', 'append');
        end
        
    end
    
    % Info
    fprintf(1, 'Processed file %4d of %4d.\n', t, numel(files));

end

